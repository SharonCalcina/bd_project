'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('modules', {
      module_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      starting_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      finish_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      module_title_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'module_titles',
          key: 'module_title_id'
        }
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('modules');
  }
};