'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('trainee', {
      trainee_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      group_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'groups',
          key: 'group_id'
        }
      },
      specialty_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'specialties',
          key: 'specialty_id'
        }
      },
      ht_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'head_trainer',
          key: 'ht_id'
        }
      },
      person_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'person',
          key: 'person_id'
        }
      },
      enrollment: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('trainee');
  }
};