'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('mt_module', {
      mt_module_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      module_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'modules',
          key: 'module_id'
        }
      },
      mt_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'module_trainer',
          key: 'mt_id'
        }
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('mt_module');
  }
};