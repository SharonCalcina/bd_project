'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('meetings_ht_trainee', {
      meeting_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ht_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'head_trainer',
          key: 'ht_id'
        }
      },
      trainee_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'trainee',
          key: 'trainee_id'
        }
      },
      schedule_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'schedules',
          key: 'schedule_id'
        }
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('meetings_ht_trainee');
  }
};