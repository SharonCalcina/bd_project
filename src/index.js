import route from "../routes/index"

require(`dotenv`).config()
const routes = require(`../routes/index`)

const PORT = 4001

app.use(session({ secret: `secret`, resave: true, saveUninitialized: true }))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(`/person`, routes)
app.listen(PORT, () => { console.log(`Server on port`, PORT) })
export default app