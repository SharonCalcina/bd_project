'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class trainee_schedule extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  trainee_schedule.init({
    /*trainee_schedule_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },*/
    schedule_id: DataTypes.INTEGER,
    trainee_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'trainee_schedule',
    tableName: 'trainee_schedule',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "trainee_schedule_pkey",
        unique: true,
        fields: [
          { name: "trainee_schedule_id" },
        ]
      },
    ]
  });
  return trainee_schedule;
};