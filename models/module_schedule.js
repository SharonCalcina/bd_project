'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class module_schedule extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  module_schedule.init({
    module_schedule_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    module_id: DataTypes.INTEGER,
    schedule_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'module_schedule',
    tableName: 'module_schedule',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "module_schedule_pkey",
        unique: true,
        fields: [
          { name: "module_schedule_id" },
        ]
      },
    ]
  });
  return module_schedule;
};