'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class meetings_ht_trainee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.meetings_ht_trainee.belongsTo(
        models.schedules,
        {
          foreignKey: {
            name: 'schedule_id',
            as: 'schedules'
          }
        }
      );
      models.meetings_ht_trainee.belongsTo(
        models.trainee,
        {
          foreignKey: {
            name: 'trainee_id',
            as: 'trainee'
          }
        }
      );
      models.meetings_ht_trainee.belongsTo(
        models.head_trainer,
        {
          foreignKey: {
            name: 'ht_id',
            as: 'head_trainer'
          }
        }
      );
    }
  }
  meetings_ht_trainee.init({
    meeting_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    ht_id: DataTypes.INTEGER,
    trainee_id: DataTypes.INTEGER,
    schedule_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'meetings_ht_trainee',
    tableName: 'meetings_ht_trainee',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "meetings_ht_trainee_pkey",
        unique: true,
        fields: [
          { name: "meeting_id" },
        ]
      },
    ]
  });
  return meetings_ht_trainee;
};