'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class groups extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.groups.hasMany(
        models.trainee,
        {
          foreignKey: { name: 'group_id' }
        }
      );
    }
  }
  groups.init({
    group_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    group_name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'groups',
    tableName: 'groups',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "groups_pkey",
        unique: true,
        fields: [
          { name: "group_id" },
        ]
      },
    ]
  });
  return groups;
};