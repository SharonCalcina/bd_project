'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class mt_module extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

    }
  }
  mt_module.init({
    mt_module_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    module_id: DataTypes.INTEGER,
    mt_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'mt_module',
    tableName: 'mt_module',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "mt_module_pkey",
        unique: true,
        fields: [
          { name: "mt_module_id" },
        ]
      },
    ]
  });
  return mt_module;
};