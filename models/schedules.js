'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class schedules extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.schedules.belongsToMany(
        models.trainee,
        {
          foreignKey: {
            name: 'schedule_id'
          }, through: models.trainee_schedule, as: 'trainee'
        }
      );
      models.schedules.belongsToMany(
        models.modules,
        {
          foreignKey: {
            name: 'schedule_id'
          }, through: models.module_schedule, as: 'modules'
        }
      );
      models.schedules.hasMany(
        models.meetings_ht_trainee,
        {
          foreignKey: { name: 'meeting_id' }
        }
      );
    }
  }
  schedules.init({
    schedule_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    time_from: DataTypes.TIME,
    time_to: DataTypes.TIME
  }, {
    sequelize,
    modelName: 'schedules',
    tableName: 'schedules',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "schedules_pkey",
        unique: true,
        fields: [
          { name: "schedule_id" },
        ]
      },
    ]
  });
  return schedules;
};