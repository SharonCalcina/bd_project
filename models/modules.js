'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class modules extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.modules.belongsTo(
        models.module_titles,
        {
          foreignKey: {
            name: 'module_title_id',
            as: 'module_titles'
          }
        }
      );
      models.modules.belongsToMany(
        models.module_trainer,
        {
          foreignKey: {
            name: 'module_id'
          }, through: models.mt_module, as: 'module_trainer'
        }
      );
      models.modules.hasMany(
        models.classes,
        {
          foreignKey: { name: 'module_id' }
        }
      );
      models.modules.belongsToMany(
        models.schedules,
        {
          foreignKey: {
            name: 'module_id'
          }, through: models.module_schedule, as: 'schedules'
        }
      );
    }
  }
  modules.init({
    module_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    starting_date: DataTypes.DATEONLY,
    finish_date: DataTypes.DATEONLY,
    module_title_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'modules',
    tableName: 'modules',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "modules_pkey",
        unique: true,
        fields: [
          { name: "module_id" },
        ]
      },
    ]
  });
  return modules;
};