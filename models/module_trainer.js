'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class module_trainer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.module_trainer.belongsTo(
        models.person,
        {
          foreignKey: {
            name: 'person_id',
            as: 'person'
          }
        }
      );
      models.module_trainer.belongsToMany(
        models.modules,
        {
          foreignKey: {
            name: 'mt_id'
          }, through: models.mt_module, as: 'modules'
        }
      );
      models.module_trainer.belongsToMany(
        models.trainee,
        {
          foreignKey: {
            name: 'mt_id'
          }, through: models.trainee_mt, as: 'trainee'
        }
      );
    }
  }
  module_trainer.init({
    mt_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    person_id: DataTypes.INTEGER,
    score: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'module_trainer',
    tableName: 'module_trainer',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "module_trainer_pkey",
        unique: true,
        fields: [
          { name: "mt_id" },
        ]
      },
    ]
  });
  return module_trainer;
};