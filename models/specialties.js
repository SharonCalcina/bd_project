'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class specialties extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.specialties.hasMany(
        models.trainee,
        {
          foreignKey: { name: 'specialty_id' }
        }
      );
    }
  }
  specialties.init({
    specialty_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    specialty_name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'specialties',
    tableName: 'specialties',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "specialties_pkey",
        unique: true,
        fields: [
          { name: "specialty_id" },
        ]
      },
    ]
  });
  return specialties;
};