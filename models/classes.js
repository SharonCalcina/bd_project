'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class classes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.classes.belongsTo(
        models.modules,
        {
          foreignKey: {
            name: 'module_id',
            as: 'modules'
          }
        }
      );
      models.classes.belongsToMany(
        models.trainee,
        {
          foreignKey: {
            name: 'class_id'
          }, through: models.class_trainee_score, as: 'trainee'
        }
      );
    }
  }
  classes.init({
    class_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    module_id: DataTypes.INTEGER,
    class_score: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'classes',
    tableName: 'classes',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "classes_pkey",
        unique: true,
        fields: [
          { name: "class_id" },
        ]
      },
    ]
  });
  return classes;
};