'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class trainee_mt extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  trainee_mt.init({
    trainee_mt_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    trainee_id: DataTypes.INTEGER,
    mt_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'trainee_mt',
    tableName: 'trainee_mt',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "trainee_mt_pkey",
        unique: true,
        fields: [
          { name: "trainee_mt_id" },
        ]
      },
    ]
  });
  return trainee_mt;
};