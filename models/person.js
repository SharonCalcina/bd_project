'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class person extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.person.belongsTo(
        models.cities,
        {
          foreignKey: {
            name: 'city_id',
            as: 'cities'
          }
        }
      );
      models.person.hasOne(
        models.module_trainer,
        {
          foreignKey: { name: 'person_id' }
        }
      );
      models.person.hasOne(
        models.head_trainer,
        {
          foreignKey: { name: 'person_id' }
        }
      );
      models.person.hasOne(
        models.trainee,
        {
          foreignKey: { name: 'person_id' }
        }
      );
    }
  }
  person.init({
    person_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    city_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    middle_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    dob: DataTypes.DATE,
    email: DataTypes.STRING,
    cellphone: DataTypes.STRING,
    phone_number: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'person',
    tableName: 'person',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "person_pkey",
        unique: true,
        fields: [
          { name: "person_id" },
        ]
      },
    ]
  });
  return person;
};