'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class trainee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.trainee.belongsTo(
        models.person,
        {
          foreignKey: {
            name: 'person_id',
            as: 'person'
          }
        }
      );
      models.trainee.belongsTo(
        models.head_trainer,
        {
          foreignKey: {
            name: 'ht_id',
            as: 'head_trainer'
          }
        }
      );
      models.trainee.belongsToMany(
        models.classes,
        {
          foreignKey: {
            name: 'trainee_id'
          }, through: models.class_trainee_score, as: 'classes'
        }
      );
      models.trainee.belongsToMany(
        models.module_trainer,
        {
          foreignKey: {
            name: 'trainee_id'
          }, through: models.trainee_mt, as: 'module_trainer'
        }
      );
      models.trainee.belongsToMany(
        models.schedules,
        {
          foreignKey: {
            name: 'trainee_id'
          }, through: models.trainee_schedule, as: 'schedules'
        }
      );
      models.trainee.hasMany(
        models.meetings_ht_trainee,
        {
          foreignKey: {
            name: 'trainee_id'
          }
        }
      );
      models.trainee.belongsTo(
        models.groups,
        {
          foreignKey: {
            name: 'group_id',
            as: 'groups'
          }
        }
      );
      models.trainee.belongsTo(
        models.specialties,
        {
          foreignKey: {
            name: 'specialty_id',
            as: 'specialties'
          }
        }
      );
    }
  }
  trainee.init({
    trainee_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    group_id: DataTypes.INTEGER,
    specialty_id: DataTypes.INTEGER,
    ht_id: DataTypes.INTEGER,
    person_id: DataTypes.INTEGER,
    enrollment: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'trainee',
    tableName: 'trainee',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "trainee_pkey",
        unique: true,
        fields: [
          { name: "trainee_id" },
        ]
      },
    ]
  });
  return trainee;
};