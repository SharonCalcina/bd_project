'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class module_titles extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.module_titles.hasMany(
        models.modules,
        {
          foreignKey: {
            name: 'module_title_id',
          }
        }
      );
    }
  }
  module_titles.init({
    module_title_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    module_name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'module_titles',
    tableName: 'module_titles',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "module_titles_pkey",
        unique: true,
        fields: [
          { name: "module_titles_id" },
        ]
      },
    ]
  });
  return module_titles;
};