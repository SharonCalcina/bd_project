'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class head_trainer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.head_trainer.belongsTo(
        models.person,
        {
          foreignKey: {
            name: 'person_id',
            as: 'person'
          }
        }
      );
      models.head_trainer.hasMany(
        models.trainee,
        {
          foreignKey: { name: 'ht_id' }
        }
      );
      models.head_trainer.hasMany(
        models.meetings_ht_trainee,
        {
          foreignKey: {
            name: 'ht_id'
          }
        }
      );
    }
  }
  head_trainer.init({
    ht_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    person_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'head_trainer',
    tableName: 'head_trainer',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "head_trainer_pkey",
        unique: true,
        fields: [
          { name: "ht_id" },
        ]
      },
    ]
  });
  return head_trainer;
};