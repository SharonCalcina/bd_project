'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class class_trainee_score extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  class_trainee_score.init({
    class_score_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    trainee_id: DataTypes.INTEGER,
    class_id: DataTypes.INTEGER,
    score: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'class_trainee_score',
    tableName: 'class_trainee_score',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "class_trainee_score_pkey",
        unique: true,
        fields: [
          { name: "class_score_id" },
        ]
      },
    ]
  });
  return class_trainee_score;
};