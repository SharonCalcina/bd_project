require('dotenv').config()
module.exports = {
  "development": {
    "username": process.env.DB_USER,
    "password": "78799034",
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOST,
    "dialect": process.env.DIALECT,
    "port": 5433
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}
