var models = require('../models/index');
const postPerson = async (req, res) => {
    const { city_id, name, middle_name, last_name, dob, email,
        cellphone, phone_number } = req.body
    const person = await models.person.create({
        city_id: city_id,
        name: name,
        middle_name: middle_name,
        last_name: last_name,
        dob: dob,
        email: email,
        cellphone: cellphone,
        phone_number: phone_number
    })
    res.status(201).send(person)
}//---Get trainer (MT) of a module---
const getMTofModule = async (req, res) => {
    const id = req.params.id
    try {
        const mt_module = await db.mt_module.findAll({
            where: {
                module_id: id
            }
        });
        var result = []
        for (let index = 0; index < mt_module.length; index++) {
            const element = mt_module[index];
            const module_trainer = await db.module_trainer.findOne({
                where: {
                    mt_id: element['dataValues'].mt_id
                }
            });
            const person = await module_trainer.getPerson();
            result.push({
                ...module_trainer['dataValues'],
                ...person['dataValues']
            })
        }
        res.status(200).send(result);
    } catch (error) {
        res.send(error)
    }
}
//---Get head trainer (HT) of a trainee---
const getHTofTrainee = async (req, res) => {
    const id = req.params.id
    try {
        const trainee = await db.trainee.findOne({
            where: {
                trainee_id: id
            }
        });
        const head_trainer = await trainee.getHead_trainer();
        const person = await head_trainer.getPerson();
        res.status(200).send({
            ...head_trainer['dataValues'],
            ...person['dataValues']
        });
    } catch (error) {
        res.send(error)
    }
}
//---Get Modules from Trainee---
const getModulesOfTrainee = async (req, res) => {
    const id = req.params.id
    try {
        var result = []
        const trainee = await db.trainee.findOne({
            where: {
                trainee_id: id
            }
        });
        const classes = await trainee.getClasses();
        var modules_id = []
        for (let index = 0; index < classes.length; index++) {
            const modules = await db.modules.findOne({
                where: {
                    module_id: classes[index]['dataValues'].module_id
                }
            });
            if (!modules_id.includes(modules.module_id)) {
                modules_id.push(modules.module_id)
                const module_title = await db.module_titles.findOne({
                    where: {
                        module_title_id: modules.module_title_id
                    }
                });
                result.push({
                    ...module_title['dataValues'],
                    ...modules['dataValues']
                })
            }
        }
        res.status(200).send(result);
    } catch (error) {
        res.send(error)
    }
}
//---- GET final score (by theme) of each trainee in a module----
const getScoreTraineeOfModule = async (req, res) => {
    const id = req.params.id
    const idm = req.params.idm

    try {
        const modules = await db.modules.findOne({
            where: {
                module_id: idm
            }
        });
        const title = await db.module_titles.findOne({
            where: {
                module_title_id: modules.module_title_id
            }
        });
        const trainee = await db.trainee.findOne({
            where: {
                trainee_id: id
            }
        });
        const classes = await trainee.getClasses({
            where: {
                module_id: idm
            }
        })
        var score = 0
        for (let index = 0; index < classes.length; index++) {
            score += classes[index]['class_trainee_score'].score
        }
        const person = await trainee.getPerson()
        // console.log(person)
        res.status(200).send({ score, ...person['dataValues'], ...title['dataValues'] });
    } catch (error) {
        res.send(error)
    }
}
//---Trainee with ID? is having classes in DATE? and TIME?
const isGettingClasses = async (req, res) => {
    const id = req.params.id
    const { date, time } = req.body

    console.log(date, time)
    try {
        const trainee = await db.trainee.findOne({
            where: {
                trainee_id: id
            }
        });
        console.log(trainee)
        var modules_id = []
        const classes = await trainee.getClasses()
        for (let index = 0; index < classes.length; index++) {
            if (!modules_id.includes(classes[index].module_id)) {
                modules_id.push(classes[index].module_id)
            }
        }
        var getClasses = false
        for (let j = 0; j < modules_id.length; j++) {
            const modules = await db.modules.findOne({
                where: {
                    module_id: modules_id[j]
                }
            })
            const ini = new Date(modules.starting_date).toLocaleDateString('sv')
            const fin = new Date(modules.finish_date).toLocaleDateString('sv')
            console.log(ini, fin)
            if (date > ini && date < fin) {
                const schedule = await modules.getSchedules()
                for (let index = 0; index < schedule.length; index++) {
                    if (time > schedule[index].time_from && time < schedule[index].time_to) {
                        getClasses = true
                    }
                }
            }
        }
        console.log(getClasses)
        if (getClasses) {
            res.status(200).send(
                'The trainee is getting classes'
            );
        } else {
            res.status(200).send(
                'The trainee is NOT getting classes'
            );
        }
    } catch (error) {
        res.send(error)
    }
}
//--- A trainee can become a MT

const createPerson = async (req, res) => {
    try {
        const person = await db.person.create({
            city_id: 4,
            name: 'Eloisa',
            middle_name: 'Julia',
            last_name: 'Merida Gutierrez',
            dob: '1987-05-21',
            email: 'eloisa.merida@gmail.com',
            cellphone: '68977452',
            createdAt: new Date(),
            updatedAt: new Date()
        });
        res.status(200).send(person);
    } catch (error) {
        res.send(error)
    }
}
const createTrainee = async (req, res) => {
    try {
        const trainee = await db.trainee.create({
            group_id: 12,
            specialty_id: 2,
            ht_id: 3,
            person_id: 16,
            enrollment: '2022-12-30'
        });
        res.status(200).send(trainee);
    } catch (error) {
        res.send(error)
    }
}
const traineeToMT = async (req, res) => {
    const id = req.params.id
    //let t
    try {
        //t = await Sequelize.Transaction();
        const trainee = await db.trainee.findOne({
            where: {
                trainee_id: id
            }
        },)
        const person = await trainee.getPerson()
        await db.trainee.destroy({
            where: {
                trainee_id: id
            }
        });
        const mt = await db.module_trainer.create({ person_id: person.person_id, score: 0 });
        const mt_module = await db.mt_module.create({ module_id: 10, mt_id: mt.mt_id })
        const trainee_mt = await db.trainee_mt.bulkCreate([
            { trainee_id: 5, mt_id: mt.mt_id },
            { trainee_id: 6, mt_id: mt.mt_id },
            { trainee_id: 7, mt_id: mt.mt_id },
            { trainee_id: 8, mt_id: mt.mt_id }]
        )
        //await t.commit();
        res.status(200).send(person, mt, mt_module, trainee_mt);
    } catch (error) {
        /*if (t) {
            await t.rollback();
        }*/
        res.send(error)
    }
}
module.exports = {
    getMTofModule, getHTofTrainee, getModulesOfTrainee, getScoreTraineeOfModule,
    isGettingClasses, createPerson, createTrainee, traineeToMT
}