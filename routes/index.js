const services = require('../services/services')
const { Router } = require('express')
const route = Router()


route
    .get('/mtModule/:id', services.getMTofModule)
    .get('/htTrainer/:id', services.getHTofTrainee)
    .get('/mod/:id', services.getModulesOfTrainee)
    .get('/finalScore/:id/:idm', services.getScoreTraineeOfModule)
    .get('/hasClasses/:id', services.isGettingClasses)
    .post('/createPerson', services.createPerson)
    .post('/createTrainee', services.createTrainee)
    .get('/traineeToMT/:id', services.traineeToMT)

module.exports = route