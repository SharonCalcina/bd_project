'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('person', [
      {
        city_id: 1,
        name: 'Sharon',
        middle_name: 'Alejandra',
        last_name: 'Calcina',
        dob: '1993-10-01',
        email: 'sharon.calcina@gmail.com',
        cellphone: '75896425',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 1,
        name: 'Diego',
        middle_name: 'Rafael',
        last_name: 'Sanabria Peñaloza',
        dob: '1992-02-15',
        email: 'diego.sanabria@training.assuresoft.com',
        cellphone: '63599924',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 3,
        name: 'Edwin',
        middle_name: 'Alfredo',
        last_name: 'Vaca',
        dob: '1996-09-08',
        email: 'edwin.vaca@training.assuresoft.com',
        cellphone: '69875124',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        city_id: 9,
        name: 'Juan',
        middle_name: 'Alfredo',
        last_name: 'Perez',
        dob: '1994-03-26',
        email: 'juan.perez@training.assuresoft.com',
        cellphone: '78954177',
        phone_number: '4561237',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 5,
        name: 'Deymar',
        middle_name: 'Rene',
        last_name: 'Torrez Montero',
        dob: '1991-05-12',
        email: 'deymar.torrez@training.assuresoft.com',
        cellphone: '74517523',
        phone_number: '4561235',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 7,
        name: 'Johann',
        middle_name: 'Josue',
        last_name: 'Geisser',
        dob: '1996-01-24',
        email: 'johann.josue@training.assuresoft.com',
        cellphone: '73222143',
        phone_number: '4561232',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 4,
        name: 'Elisa',
        middle_name: 'Alfredo',
        last_name: 'Ruiz Olivares',
        dob: '2000-03-25',
        email: 'elisa.ruiz@trueforce.com',
        cellphone: '63659885',
        phone_number: '4125457',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 5,
        name: 'Patrick',
        middle_name: 'Omar',
        last_name: 'Nina Gutierrez',
        dob: '2005-10-03',
        email: 'patrik.nina@training.assuresoft.com',
        cellphone: '63902011',
        phone_number: '4532656',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 7,
        name: 'Juan',
        middle_name: 'Jose',
        last_name: 'Flores Barbados',
        dob: '1991-05-14',
        email: 'juan.flores@trueforce.com',
        cellphone: '78912057',
        phone_number: '4458722',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 2,
        name: 'Luis',
        middle_name: 'Fernando',
        last_name: 'Calderon',
        dob: '1998-05-22',
        email: 'luis.calderon@trueforce.com',
        cellphone: '78795412',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 1,
        name: 'Monica',
        middle_name: 'Patricia',
        last_name: 'Olmos Parra',
        dob: '1991-10-05',
        email: 'monica.olmos@training.assuresoft.com',
        cellphone: '66521447',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 2,
        name: 'Erick',
        middle_name: 'Roman',
        last_name: 'Duran Clavijo',
        dob: '1997-01-18',
        email: 'erick.duran@trueforce.com',
        cellphone: '76988541',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 1,
        name: 'Cinthia',
        last_name: 'Rojas Rodas',
        dob: '2001-05-23',
        email: 'cinthia.rojas@trueforce.com',
        cellphone: '63659885',
        phone_number: '2561659',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 1,
        name: 'Jaime',
        last_name: 'Gemio Paredes',
        dob: '1985-06-18',
        email: 'jaime.gemio@training.assuresoft.com',
        cellphone: '63299874',
        phone_number: '2225641',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        city_id: 1,
        name: 'Olivia',
        last_name: 'Casas Hurtado',
        dob: '1991-05-14',
        email: 'olivia.casas@trueforce.com',
        cellphone: '78995610',
        phone_number: '2369874',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('person', null, {});
  }
};
