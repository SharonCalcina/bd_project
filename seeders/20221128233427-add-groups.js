'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('groups', [
      { group_name: 'team_dev_group1' },
      { group_name: 'team_dev_group2' },
      { group_name: 'team_dev_group3' },
      { group_name: 'team_dev_group4' },
      { group_name: 'team_dev_group5' },
      { group_name: 'team_dev_group6' },
      { group_name: 'team_dev_group7' },
      { group_name: 'team_dev_group8' },
      { group_name: 'team_dev_group9' },
      { group_name: 'team_dev_group10' },
      { group_name: 'team_qa_group1' },
      { group_name: 'team_qa_group2' },
      { group_name: 'team_qa_group3' },
      { group_name: 'team_qa_group4' },
      { group_name: 'team_qa_group5' }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('groups', null, {});
  }
};
