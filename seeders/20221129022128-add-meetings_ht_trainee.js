'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('meetings_ht_trainee', [
      { ht_id: 1, trainee_id: 1, schedule_id: 7 },
      { ht_id: 1, trainee_id: 4, schedule_id: 12 },
      { ht_id: 1, trainee_id: 2, schedule_id: 15 },
      { ht_id: 1, trainee_id: 3, schedule_id: 15 },
      { ht_id: 2, trainee_id: 5, schedule_id: 17 },
      { ht_id: 2, trainee_id: 7, schedule_id: 17 },
      { ht_id: 2, trainee_id: 6, schedule_id: 16 },
      { ht_id: 2, trainee_id: 8, schedule_id: 2 }
    ], {});
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('meetings_ht_trainee', null, {});
  }
};
