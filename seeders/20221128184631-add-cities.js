'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    //Example:
    await queryInterface.bulkInsert('cities', [
      { city_name: 'La Paz', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Cochabamba', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Santa Cruz', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Oruro', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Potosi', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Pando', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Beni', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Chuquisaca', createdAt: new Date(), updatedAt: new Date() },
      { city_name: 'Tarija', createdAt: new Date(), updatedAt: new Date() },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('cities', null, {});
  }
};
