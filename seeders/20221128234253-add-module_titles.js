'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('module_titles', [
      { module_name: 'Development Methodologies' },
      { module_name: 'Software Development Fundamentals' },
      { module_name: 'Quality Assurance Fundamentals' },
      { module_name: 'Database Fundamentals' },
      { module_name: 'Software Architecture Fundamentals' },
      { module_name: 'Software Architecture II' },
      { module_name: 'Backend Development' },
      { module_name: 'Frontend Web Development' },
      { module_name: 'Cloud Computing' },
      { module_name: 'Mobile Development' },
      { module_name: 'Dev OPS' },


    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('module_titles', null, {});
  }
};
