'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('trainee', [
      { group_id: 3, specialty_id: 1, ht_id: 2, person_id: 1, enrollment: '2022-10-12' },
      { group_id: 3, specialty_id: 1, ht_id: 1, person_id: 2, enrollment: '2022-09-10 ' },
      { group_id: 2, specialty_id: 1, ht_id: 1, person_id: 5, enrollment: '2022-04-23' },
      { group_id: 2, specialty_id: 1, ht_id: 2, person_id: 6, enrollment: '2022-10-12' },
      { group_id: 3, specialty_id: 1, ht_id: 2, person_id: 3, enrollment: '2022-07-09' },
      { group_id: 3, specialty_id: 1, ht_id: 1, person_id: 4, enrollment: '2022-09-10 ' },
      { group_id: 2, specialty_id: 1, ht_id: 2, person_id: 7, enrollment: '2022-04-23' },
      { group_id: 2, specialty_id: 1, ht_id: 1, person_id: 8, enrollment: '2022-07-09' },

    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('trainee', null, {});
  }
};
