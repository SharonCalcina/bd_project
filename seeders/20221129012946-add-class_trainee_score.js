'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('class_trainee_score', [
      { trainee_id: 1, class_id: 1, score: 5 },
      { trainee_id: 1, class_id: 2, score: 5 },
      { trainee_id: 1, class_id: 3, score: 2 },
      { trainee_id: 2, class_id: 1, score: 3 },
      { trainee_id: 2, class_id: 2, score: 5 },
      { trainee_id: 3, class_id: 1, score: 4 },
      { trainee_id: 3, class_id: 2, score: 1 },
      { trainee_id: 4, class_id: 1, score: 0 },
      { trainee_id: 4, class_id: 2, score: 3 },
      { trainee_id: 4, class_id: 3, score: 5 }
    ], {});
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('class_trainee_score', null, {});
  }
};
