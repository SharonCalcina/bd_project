'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('classes', [
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 1, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 2, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 3, class_score: 5.0 },
      { module_id: 4, class_score: 5.0 },
      { module_id: 4, class_score: 5.0 },
      { module_id: 4, class_score: 5.0 },
      { module_id: 4, class_score: 5.0 },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('classes', null, {});

  }
};
