'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('trainee_mt', [
      { trainee_id: 1, mt_id: 1 },
      { trainee_id: 2, mt_id: 1 },
      { trainee_id: 5, mt_id: 1 },
      { trainee_id: 6, mt_id: 1 },
      { trainee_id: 3, mt_id: 2 },
      { trainee_id: 4, mt_id: 2 },
      { trainee_id: 7, mt_id: 2 },
      { trainee_id: 8, mt_id: 2 },
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('trainee_mt', null, {});
  }
};
