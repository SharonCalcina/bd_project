'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('trainee_schedule', [
      { schedule_id: 20, trainee_id: 1 },
      { schedule_id: 20, trainee_id: 2 },
      { schedule_id: 20, trainee_id: 3 },
      { schedule_id: 20, trainee_id: 4 },
      { schedule_id: 21, trainee_id: 5 },
      { schedule_id: 21, trainee_id: 6 },
      { schedule_id: 21, trainee_id: 7 },
      { schedule_id: 21, trainee_id: 8 }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('trainee_schedule', null, {});
  }
};
