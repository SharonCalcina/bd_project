'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('module_schedule', [
      { module_id: 1, schedule_id: 1 },
      { module_id: 1, schedule_id: 3 },
      { module_id: 2, schedule_id: 1 },
      { module_id: 3, schedule_id: 1 },
      { module_id: 3, schedule_id: 5 },
      { module_id: 4, schedule_id: 2 },
      { module_id: 4, schedule_id: 3 },
      { module_id: 5, schedule_id: 2 }
    ], {});
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('module_schedule', null, {});
  }
};