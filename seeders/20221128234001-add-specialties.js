'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('specialties', [
      { specialty_name: 'Developer' },
      { specialty_name: 'QA' },
    ], {});

  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('specialties', null, {});

  }
};
