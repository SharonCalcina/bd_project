'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('schedules', [
      { time_from: '8:30', time_to: '10:00' },
      { time_from: '10:00', time_to: '11:30' },
      { time_from: '14:30', time_to: '16:00' },
      { time_from: '16:00', time_to: '17:30' },
      { time_from: '17:30', time_to: '19:00' },
      { time_from: '8:30', time_to: '9:00' },
      { time_from: '9:00', time_to: '9:30' },
      { time_from: '09:30', time_to: '10:00' },
      { time_from: '10:00', time_to: '10:30' },
      { time_from: '10:30', time_to: '11:00' },
      { time_from: '11:00', time_to: '11:30' },
      { time_from: '14:30', time_to: '15:00' },
      { time_from: '15:00', time_to: '15:30' },
      { time_from: '15:30', time_to: '16:00' },
      { time_from: '16:00', time_to: '16:30' },
      { time_from: '16:30', time_to: '17:00' },
      { time_from: '17:00', time_to: '17:30' },
      { time_from: '17:30', time_to: '18:00' },
      { time_from: '18:00', time_to: '18:30' },
      { time_from: '8:00', time_to: '18:00' },
      { time_from: '8:30', time_to: '18:30' },
      { time_from: '9:00', time_to: '19:00' },
      { time_from: '9:30', time_to: '19:30' }
    ], {});
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('schedules', null, {});
  }
};
