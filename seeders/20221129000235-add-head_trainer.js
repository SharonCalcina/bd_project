'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('head_trainer', [
      { person_id: 9 },
      { person_id: 10 },
      { person_id: 11 }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('head_trainer', null, {});
  }
};
