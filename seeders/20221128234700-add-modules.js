'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.bulkInsert('modules', [
      { module_title_id: 1, starting_date: '2021-10-01', finish_date: '2021-10-22' },
      { module_title_id: 2, starting_date: '2021-10-25', finish_date: '2021-11-12' },
      { module_title_id: 4, starting_date: '2021-11-15', finish_date: '2021-12-03' },
      { module_title_id: 5, starting_date: '2021-12-07', finish_date: '2021-12-28' },
      { module_title_id: 7, starting_date: '2022-12-29', finish_date: '2022-01-14' },
      { module_title_id: 8, starting_date: '2022-01-17', finish_date: '2022-02-11' },
      { module_title_id: 10, starting_date: '2022-03-07', finish_date: '2022-03-25' },
      { module_title_id: 11, starting_date: '2022-03-28', finish_date: '2022-04-08' },
      { module_title_id: 1, starting_date: '2022-02-15', finish_date: '2021-03-01' },
      { module_title_id: 2, starting_date: '2022-03-02', finish_date: '2022-03-16' },
      { module_title_id: 3, starting_date: '2022-03-17', finish_date: '2022-03-31' },
      { module_title_id: 4, starting_date: '2022-04-01', finish_date: '2022-04-22' },
      { module_title_id: 5, starting_date: '2022-04-25', finish_date: '2022-05-20' },
      { module_title_id: 8, starting_date: '2022-05-23', finish_date: '2022-06-13' },
      { module_title_id: 10, starting_date: '2022-06-14', finish_date: '2022-07-08' },
      { module_title_id: 11, starting_date: '2022-07-11', finish_date: '2022-08-05' }
    ], {});

  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('modules', null, {});
  }
};
