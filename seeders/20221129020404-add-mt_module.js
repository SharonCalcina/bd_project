'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('mt_module', [
      { module_id: 3, mt_id: 1 },
      { module_id: 4, mt_id: 1 },
      { module_id: 2, mt_id: 2 },
      { module_id: 1, mt_id: 3 },
      { module_id: 1, mt_id: 4 },
      { module_id: 9, mt_id: 4 },
      { module_id: 3, mt_id: 4 }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('mt_module', null, {});
  }
};
