'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('module_trainer', [
      { person_id: 12, score: 85 },
      { person_id: 13, score: 92.4 },
      { person_id: 14, score: 65.5 },
      { person_id: 15, score: 84.7 }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('module_trainer', null, {});
  }
};
